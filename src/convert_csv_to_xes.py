import os
import pandas as pd
from plotnine import ggplot, geom_point, aes, theme_xkcd, element_text, theme, theme_seaborn

from pm4py.objects.log.adapters.pandas import csv_import_adapter

from pm4py_wrapper.helper import read_csv_into_event_stream, convert_event_stream_to_log, write_xes


def convert_csv_to_xes(input_path: str, output_path):
    event_stream = read_csv_into_event_stream(input_path)
    log = convert_event_stream_to_log(event_stream)
    write_xes(log, output_path)


def create_dotted_graph(csv_path):
    # timestamp on X, trace concept:name = Y
    data = csv_import_adapter.import_dataframe_from_path(csv_path, sep=';')
    data['Timestamp'] = pd.to_datetime(data['Timestamp'], format='%d-%m-%Y:%H.%M')

    fig = (
            ggplot(data, aes('Timestamp', 'Case ID', color='Activity'))
            + geom_point()
            + theme(axis_text_x=element_text(rotation=45, hjust=1))
    )
    fig.draw().savefig('plot.pdf', bbox_inches='tight')
    print('test')
    pass


def main():
    input_path = os.path.join('./', 'test_data', 'running-example.csv')
    output_path = os.path.join('./', 'out', 'running-example.xes')

    # convert_csv_to_xes(os.path.abspath(input_path), os.path.abspath(output_path))
    create_dotted_graph(input_path)


if __name__ == '__main__':
    main()
