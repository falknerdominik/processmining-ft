import os

import streamlit as st
from plotnine import ggplot, geom_point, theme, element_text, aes, scale_color_brewer, geom_bar, geom_col, coord_flip, \
    geom_text, ylab, xlab
from pm4py.objects.log.adapters.pandas import csv_import_adapter
import pandas as pd


@st.cache()
def load_data(csv_path):
    data = csv_import_adapter.import_dataframe_from_path(csv_path, sep=',')
    data['time:timestamp'] = pd.to_datetime(data['time:timestamp'], format='%Y-%d-%m %H:%M:%S%z')
    return data


def draw_dotted_chart(x_column_name, y_column_name, color_column_name):
    if color_column_name is None:
        dotted_chart = ggplot(data, aes(x_column_name, y_column_name))
    else:
        dotted_chart = ggplot(data, aes(x_column_name, y_column_name, color=color_column_name))

    dotted_chart = (
            dotted_chart
            + geom_point()
            + theme(axis_text_x=element_text(rotation=45, ha='right'))
            + scale_color_brewer(type='qual', palette=7)
    )
    return dotted_chart.draw()


# load data
# input_path = os.path.join('./', '..', '..', 'test_data', 'running-example.csv')
input_path = os.path.join('./', '..', '..', 'test_data', 'Road_Traffic_Fine_Management_Process.csv')
data = load_data(os.path.abspath(input_path))

# playground for testing out with dotted chart
if st.checkbox('Show Playground'):
    if st.checkbox('Show Raw Data'):
        st.subheader('Raw data')
        st.write(data)

    option_x = st.selectbox('X-Axis', data.columns, 2)
    option_y = st.selectbox('Y-Axis', data.columns, 0)
    option_color = st.selectbox('Color', data.columns, 1)

    st.pyplot(draw_dotted_chart(option_x, option_y, option_color), bbox_inches='tight')

# plot 1: 'Case ID', 'Resource', 'Activity'
# which resource is involed in which step

# plot 2: 'Costs', 'Activity', 'Activity'
# How expensive are the activities
activity_group = data.groupby('concept:name').agg(medianCost=('amount', 'median'))
activity_group['concept:name'] = activity_group.index
activity_group.sort_values(by='medianCost', inplace=True)
fig = (
        ggplot(activity_group, aes('concept:name', 'medianCost'))
        + geom_col()
        + ylab('median Costs')
        + coord_flip()
)

st.pyplot(fig.draw(), bbox_inches='tight')

fig = (
        ggplot(data, aes('concept:name'))
        + geom_bar()
        + coord_flip()
)

st.pyplot(fig.draw(), bbox_inches='tight')
# stat 1: Histogram Case Cost

if __name__ == '__main__':
    pass
