import os

from plotnine import ggplot, aes, geom_bar, geom_col, coord_flip, geom_point, position_dodge, geom_text, xlab, ylab
from pm4py.objects.log.importer.xes import factory as xes_import_factory
from pm4py.objects.log.exporter.csv import factory as csv_exporter
import pylab as plt

import pandas as pd


def xes_to_csv(in_path, output_path):
    log = xes_import_factory.apply(in_path)
    csv_exporter.export(log, output_path)


def missing_matrix(csv_path, out_path):
    data = pd.read_csv(csv_path)
    data = data.where(pd.isna(data), 0)
    data = data.fillna(1)

    row_count = data.shape[0]
    summed_cols = data.sum()
    missigness_values = summed_cols / row_count
    missigness_values = missigness_values.round(2)

    columns = missigness_values.index
    frame = pd.DataFrame({
        'x': columns.tolist(),
        'y': missigness_values.tolist(),
    })

    dodge_text = position_dodge(width=0.7)
    fig = (ggplot(frame, aes(x='x', y='y'))
           + geom_col()
           + geom_text(aes(label='y'),
                       position=dodge_text,
                       size=6, ha='left', va='center', format_string=' {}')
           + ylab("Percent of missing data")
           + xlab("Column in dataset")
           + coord_flip()
           )

    fig.draw().savefig(out_path, bbox_inches='tight')


def get_vehicle_data_avg(csv_path, out_path):
    data = pd.read_csv(csv_path)
    vehicle_data = data.loc[data['concept:name'] == 'Create Fine']

    grouped = vehicle_data.groupby('vehicleClass').agg(avgAmount=('amount', 'mean'))
    grouped.reset_index(inplace=True)
    fig = (ggplot(grouped, aes(x='vehicleClass', y='avgAmount'))
           + geom_col()
           + xlab("Vehicle Class")
           + ylab("Avg. Amount fined")
           + coord_flip()
           )

    fig.draw().savefig(out_path, bbox_inches='tight')


def get_vehicle_data_total(csv_path, out_path):
    data = pd.read_csv(csv_path)
    vehicle_data = data.loc[data['concept:name'] == 'Create Fine']

    grouped = vehicle_data.groupby('vehicleClass').agg(totalAmount=('amount', 'sum'))
    grouped.reset_index(inplace=True)
    fig = (ggplot(grouped, aes(x='vehicleClass', y='totalAmount'))
           + geom_col()
           + xlab("Vehicle Class")
           + ylab("Total Amount fined")
           + coord_flip()
           )

    fig.draw().savefig(out_path, bbox_inches='tight')


def get_number_of_events_overview(csv_path, out_path):
    data = pd.read_csv(csv_path)

    # grouped = vehicle_data.groupby('concept:name').agg(count=('amount', 'count'))
    # grouped.reset_index(inplace=True)
    fig = (ggplot(data)
           + geom_bar(aes('concept:name'))
           + xlab('')
           + ylab("Number of Events")
           + coord_flip()
           )

    fig.draw().savefig(out_path, bbox_inches='tight')


def articel_overview(csv_path, out_path_avg, out_path_median):
    data = pd.read_csv(csv_path)
    fine_data = data.loc[data['concept:name'] == 'Create Fine']
    grouped = fine_data.groupby('article').agg(avg=('amount', 'mean'), median=('amount', 'median'))
    grouped.reset_index(inplace=True)
    grouped['article'] = grouped['article'].astype(int)

    five_largest_avg = grouped.nlargest(5, 'avg')
    five_largest_avg = five_largest_avg.sort_values(by=['avg'])

    five_largest_med = grouped.nlargest(5, 'median')
    five_largest_med = five_largest_avg.sort_values(by=['median'])

    fig = (ggplot(five_largest_avg, aes(x='factor(article)', y='avg'))
           + geom_col()
           + xlab("Article")
           + ylab("Avg. Amount fined")
           + coord_flip()
           )
    fig.draw().savefig(out_path_avg, bbox_inches='tight')

    fig = (ggplot(five_largest_med, aes(x='factor(article)', y='median'))
           + geom_col()
           + xlab("Article")
           + ylab("Median Amount fined")
           + coord_flip()
           )

    fig.draw().savefig(out_path_median, bbox_inches='tight')
    print('test')


if __name__ == '__main__':
    input_path = os.path.join('./', '..', '..', 'test_data', 'Road_Traffic_Fine_Management_Process.xes')
    csv_path = os.path.join('./', '..', '..', 'test_data', 'Road_Traffic_Fine_Management_Process.csv')
    out_path = os.path.join('./', '..', '..', 'out', 'missigness.pdf')
    out_path2 = os.path.join('./', '..', '..', 'out', 'amount_per_class.pdf')
    out_path3 = os.path.join('./', '..', '..', 'out', 'number_of_events.pdf')
    out_path4 = os.path.join('./', '..', '..', 'out', 'total_per class.pdf')
    out_path_avg = os.path.join('./', '..', '..', 'out', 'avg_per_article.pdf')
    out_path_mean = os.path.join('./', '..', '..', 'out', 'median_per_article.pdf')

    # missing_matrix(os.path.abspath(input_path), os.path.abspath(out_path))
    # get_number_of_events_overview(csv_path, out_path3)
    # get_vehicle_data_avg(csv_path, out_path2)
    # get_vehicle_data_total(csv_path, out_path4)
    articel_overview(csv_path, out_path_avg, out_path_mean)
