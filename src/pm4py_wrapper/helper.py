from pm4py.objects.log.log import EventStream, EventLog
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.objects.log.exporter.xes import factory as xes_exporter
from pm4py.util import constants


def read_csv_into_event_stream(csv_path: str, separator: str = ';') -> EventStream:
    return csv_importer.import_event_stream(csv_path, parameters={'sep': separator})


def convert_event_stream_to_log(
        event_stream,
        case_id_column_name: str = 'Case ID',
        activity_column_name: str = 'Activity',
        resource_column_name: str = 'Resource',
        timestamp_column_name: str = 'Timestamp',
) -> EventLog:
    return conversion_factory.apply(event_stream, parameters={
        constants.PARAMETER_CONSTANT_CASEID_KEY: case_id_column_name,
        constants.PARAMETER_CONSTANT_ACTIVITY_KEY: activity_column_name,
        constants.PARAMETER_CONSTANT_RESOURCE_KEY: resource_column_name,
        constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: timestamp_column_name,
    })


def write_xes(log: EventLog, output_path: str):
    xes_exporter.export_log(log, output_path)
