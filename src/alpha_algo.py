import os
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.algo.discovery.alpha import factory as alpha_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory


def main():
    input_path = os.path.join('./', '..', 'out', 'running-example.xes')
    output_path = os.path.join('./', '..', 'out', 'running-example.svg')
    log = xes_importer.import_log(os.path.join(input_path))
    net, initial_marking, final_marking = alpha_miner.apply(log)

    print("PLACES {}".format(net.places))
    print("TRANSITIONS {}".format(net.transitions))

    # draw graph and save it
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking)
    pn_vis_factory.view(gviz)
    pn_vis_factory.save(gviz, output_path)


if __name__ == '__main__':
    main()
